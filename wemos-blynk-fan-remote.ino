/*************************************************************
  *
  * Control lights and fan. Wemos D1 board on remote ceiling  
  * fan remote control. Controls OFF LOW MED HIGH fan and light.
  * Also dims light. Short press on light is OFF/ON. Long 
  * press on light is DIM (alternating direction).
  *
 *************************************************************/

#define BLYNK_PRINT Serial
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

int LightPin = 16;                                  // corresponds with D0
int FanOffPin = 5;                                  // corresponds with D1
int FanLowPin = 4;                                  // corresponds with D2
int FanMedPin = 0;                                  // corresponds with D3
int FanHighPin = 12;                                // corresponds with D5

int oldFamLightState = 1;
int newFamLightState = 0;

char auth[] = "**your_blynk_auth_token";            // your Blynk auth token
char ssid[] = "**your_wifi_network**";                             // your wifi network name
char pass[] = "**your_wifi_network_password**";                      // your wifi network password

void setup() {
  pinMode(LightPin, OUTPUT);
  pinMode(FanOffPin, OUTPUT);
  pinMode(FanLowPin, OUTPUT);
  pinMode(FanMedPin, OUTPUT);
  pinMode(FanHighPin, OUTPUT);
  digitalWrite(FanMedPin, LOW);
  digitalWrite(FanHighPin, LOW);
  
  Serial.begin(9600);
  WiFi.begin(ssid, pass);  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Blynk.begin(auth, ssid, pass);
}

// control light
BLYNK_WRITE(V0) {
  int newFamLightState = param.asInt();

  if (newFamLightState == 0 && oldFamLightState == 1) {
    // Toggle light off
    digitalWrite(LightPin, HIGH);
    delay(400);
    Serial.print("off\n");
    digitalWrite(LightPin, LOW);
    oldFamLightState = newFamLightState;
  } else if (newFamLightState == 1 && oldFamLightState == 0) {
    // Toggle light on
    digitalWrite(LightPin, HIGH);
    delay(400);
    Serial.print("on\n");
    digitalWrite(LightPin, LOW);
    oldFamLightState = newFamLightState;
  }
}

// control ceiling fan
BLYNK_WRITE(V1) {
  int newFamFanState = param.asInt();

  if (newFamFanState == 1) {
    // Fan off
    digitalWrite(FanOffPin, HIGH);
    delay(750);
    Serial.print("fan off\n");
    digitalWrite(FanOffPin, LOW);
  } else if (newFamFanState == 2) {
    // Fan low
    digitalWrite(FanLowPin, HIGH);
    delay(750);
    Serial.print("fan low\n");
    digitalWrite(FanLowPin, LOW);
  } else if (newFamFanState == 3) {
    // Fan med
    digitalWrite(FanMedPin, HIGH);
    delay(750);
    Serial.print("fan med\n");
    digitalWrite(FanMedPin, LOW);
  } else if (newFamFanState == 4) {
    // Fan high
    digitalWrite(FanHighPin, HIGH);
    delay(750);
    Serial.print("fan high\n");
    digitalWrite(FanHighPin, LOW);
  }
}

void loop() {
  Blynk.run();
}

